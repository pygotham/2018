---
abstract: In the style of a modern, web-development choose your own adventure, we'll
  explore the benefits and trade offs of GraphQL and REST (including performance and
  scaling).  We'll build up an example schema using a choose your own adventure story
  as the model and leverage that schema in front end code.
duration: 30
level: Intermediate
presentation_url: https://slides.com/dvndrsn/cyoa-graphql-pygotham
room: PennTop North
slot: 2018-10-05 13:30:00-04:00
speakers:
- David Anderson
title: Choose Your Own Adventure for Client Web Services with GraphQL
type: talk
---

In this talk, we discuss Facebook’s graphql standard, an a la carte way for front end clients to consume data from the backend, the python implementation of that standard (graphine), basic queries and mutations, and some advanced techniques. We’ll work up from shared principles with traditional REST web services to the new paradigm of data specific to each client request and how this makes back end and front end developers happier and more productive.

- About me, Prologue to the Adventure
- Chapter 1 - API Architecture and Design
  - _Concerning the design and structure of a GraphQL API and its benefits over a traditional REST API._
- Chapter 2 - Scaling and Performance
  - _In which, DataLoaders save the day when N+1 queries strike._
- Chapter 3 - Building a Rich Client Web Application
  - _Wherein we learn how to tame the client side cache monster with Apollo and better API design._
- Epilogue + Questions

To learn more, check out [The Rabbit Hole Podcast][podcast] and the [episodes][ep2] [we've recorded][ep1] [about GraphQL][ep3] or keep the conversation going on [twitter][twitter].

[podcast]: https://www.stridenyc.com/podcasts
[ep1]: https://www.stridenyc.com/podcasts/79-graphql-in-python-with-patrick-arminio-pycon-italia-part-3
[ep2]: https://www.stridenyc.com/podcasts/75-graphql-hot-takes-with-stephen-meriwether
[ep3]: https://www.stridenyc.com/podcasts/52-is-2018-the-year-graphql-kills-rest
[twitter]: http://twitter.com/dvndrsn
