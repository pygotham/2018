---
abstract: Do you know what everyone would do if Godzilla suddenly showed up in the
  room? Give social scientists enough computer power, and they can tell you. It wouldn't
  look anything like a disaster movie, and the gap between Hollywood guesses and what
  people can prove is where things get exciting.
duration: 45
level: All
presentation_url: https://docs.google.com/presentation/d/14wL7ZJKWnofA4C5fn1gMFLU8if6lk4l4DxW0C2qwamo/edit?usp=sharing
room: Madison
slot: 2018-10-06 11:15:00-04:00
speakers:
- Eileen Young
title: Computer Modeling in the Social Science of Disasters
type: talk
video_url: https://youtu.be/-QB3pKikbEI
---

Computer modeling of exactly what happens during a disaster isn't just a cool way to understand things and make better disaster movies - it saves lives. And getting a social scientist involved means that it will be more accurate, because panic and perfectly rational behavior are both myths. Learn about disaster myths, exactly how social science can be used in disaster modeling, how it's being used now, and why having more software developers involved would be great for everyone.

After this talk, you should have some ideas about the ways programming and social science are working together to make the future both more exciting and more predictable - and maybe ideas for a project of your own. There will be a somewhat long question period because of the subject matter.
