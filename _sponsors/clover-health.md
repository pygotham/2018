---
name: Clover Health
tier: gold
site_url: https://www.cloverhealth.com/en/about-us/careers
logo: clover-health.png
---
We are reinventing health insurance by combining the power of data with human empathy to keep our
members healthier. We believe the healthcare system is broken, so we've created custom software and
analytics to empower our clinical staff to intervene and provide personalized care to the people who
need it most.

We put our members first, and our success as a team is measured by the quality of life of the people
we serve. Those who work at Clover are passionate and mission-driven with diverse areas of
expertise, working together to solve the most complicated problem in the world: healthcare.
