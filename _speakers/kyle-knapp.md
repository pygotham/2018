---
name: Kyle Knapp
talks:
- 'Automating Code Quality: Next Level'
---

Kyle Knapp is a Software Development Engineer at Amazon Web Services. He is a core maintainer of various Python open source project related to AWS such as the AWS CLI, Boto3, AWS Shell, and chalice.