---
name: Trey Hunner
talks:
- Python Oddities Explained
---

Trey Hunner helps Python and Django teams on-board new developers through on-site team training and sends Python exercises to learners every week through Python Morsels.

Trey is a director at the Python Software Foundation, a member of the Django Software Foundation, and is heavily involved with his local Python meetup group in San Diego.