---
abstract: Although most well-known for being the founder of the Twisted project,
  Glyph has also worked on massively multiplayer online games, dynamic web
  applications, enterprise information management software, and created or
  contributed to dozens of open source projects, mostly related to
  infrastructure.  Currently he works at Pilot.com, engineering the automation
  of bookkeeping.
name: Glyph
talks:
- "The Automatic Computer and You: A Meditation Upon The History And Future Of Software Development"
---
[Glyph](https://en.wikipedia.org/wiki/Glyph_Lefkowitz) is mostly a computer
programmer, mostly in [Python](https://www.python.org/).<!--more-->

Although most well-known for being the founder of [the Twisted
project](https://twistedmatrix.com/), Glyph has also worked on [massively
multiplayer online
games](https://en.wikipedia.org/wiki/Ultima_Worlds_Online:_Origin), [dynamic web
applications](https://github.com/twisted/quotient), [enterprise information
management software](https://www.calendarserver.org/), and created or
contributed to [dozens of open source projects](https://github.com/glyph),
mostly related to infrastructure.

He’s also a [prolific public speaker](http://pyvideo.org/speaker/glyph.html),
mostly about software, also occasionally about ethics.

He has run Python programs, and written Python programs to be run, on
mainframes, on custom-built embedded devices, and just about everything in
between.

Currently, he works at [Pilot](https://pilot.com/), making sure the numbers
always add up.

In closing, he hopes you’re having a nice day.
