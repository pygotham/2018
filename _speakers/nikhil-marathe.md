---
name: Nikhil Marathe
talks:
- A deep dive into Python stack frames
---

Nikhil is a software engineer interested in systems programming. He works at Dropbox improving the desktop client. In past careers, he has wrangled with Docker and implemented several Web APIs for Firefox. He is constantly trying to get away from the computer to spend more time climbing mountains.
