---
name: Anderson Frailey
talks:
- Open Source Policy Making - Challenges and Opportunities
---

Anderson Frailey is a core maintainer of the open source TaxData project and a frequent contributor to Tax-Calculator, an open source microsimulation tax model. He is also a Research Associate in the Open Source Policy Center at the American Enterprise Institute. In his free time, he enjoys digging into baseball data using the wonderful PyBaseball package.