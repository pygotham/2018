---
title: Social Event
---

What better way to end the first day of {{ site.data.event.name }} than with an evening of
socializing and refreshments? Join us right after Lightning Talks for a chance to hang out and talk
about your favorite parts of PyGotham, or anything else you want to share.

The {{ site.data.event.name }} Social Event is brought to you by
[Trialspark]({% link sponsors/index.md %}#trialspark) and
[IBM]({% link sponsors/index.md %}#ibm). Tunes brought to you by
[A-Ron the DJ](https://twitter.com/A_Ron_The_DJ).

Registration is free with your PyGotham ticket. Please
[RSVP]({{ site.data.event.social_event_url }}) to reserve your spot.
